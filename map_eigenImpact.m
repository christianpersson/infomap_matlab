function [ M , startPartition ] = eigenImpact( input_args )
%% The transition matrix

% i = to, j : from, Note the transpose...
M = [0 1 0 0 0 0;
    1 0  1 0 0 0;
    1 0 0 1 0 0;
    1 0 0 0 1 0;
    1 0 0 0 0 1;
    1 0 0 0 0 0];


%% The cluster initial partition
startPartition = [1 2 3 4 5 6];

fprintf('\n\n****************************************\n')

%louvain( M , [1 2 3 4 5 6], false, true);


if abs(calculate( M', ones(6,1), false, true ) - 2.145358143) > 0.00001
    fprintf('Error in unrec link\n');
    [i, m, tot] = calculate( M, ones(6,1), false, true )
end


if abs(calculate( M', [1 1 1 1 2 2], false, true ) -  2.295787654) > 0.00001
    fprintf('Error in unrec link\n');
    [i, m, tot] = calculate( M', [1 1 1 1 2 2],  false, true )
end


if abs(calculate( M', [1 2 3 4 5 6], false, true ) -  4.137967188) > 0.00001
    fprintf('Error in unrec link\n');
    [i, m, tot] = calculate( M', [1 2 3 4 5 6],  false, true )
end



end

