function [ codeLength, indexCodeLength, moduleCodeLength ] = mapEquation(modules, flow_ij, pi_i, teleport_i, recorded)
%% MAP EQUATION
% Calculate the code length in a two level network.
% 
% For more information, see mapequation.org
% <http://www.mapequation.org>


%% Initialization
ClusterPartition = cell(1,max(modules));
for i = 1 : length(modules)
    ClusterPartition{modules(i)} = [ClusterPartition{modules(i)} i];
end

ALPHA = 0.15;
numberOfClusters = length(ClusterPartition);
qEnter = zeros(1, numberOfClusters); 
qExit = zeros(1, numberOfClusters); 
moduleCodeLength = zeros(1, numberOfClusters);


%% Calculate _qEnter_and _qExit_ for each module.
for i = 1 : length(flow_ij)
    fromModule = modules(flow_ij(i,1));
    toModule = modules(flow_ij(i,2));
    w = flow_ij(i,3);
    if fromModule ~= toModule
        qExit(fromModule) = qExit(fromModule) + w;
        qEnter(toModule) = qEnter(toModule) + w;
    end
end

%% Calculate _moduleCodeLength_ for module _i_.
for i = 1 : numberOfClusters
    pEnter = 0; HP = 0; pCluster = 0;
     
    cluster = ClusterPartition{i}; %extract cluster nodes
    nodesInCluster = length(cluster);
    
    if nodesInCluster == 0
        continue;
    end
    
    % Calculate p to enter cluster i and to be in cluster i
    for alpha = 1 : nodesInCluster
        pCluster = pCluster + pi_i(cluster(alpha));
        pEnter = pEnter + teleport_i(cluster(alpha));
    end
    
    % Add teleportation probability if recorded
    if recorded
        qExit(i) = qExit(i) + pCluster * (1-pEnter) * ALPHA;
        qEnter(i) = qEnter(i) + (1-pCluster) * pEnter * ALPHA;
    end
    
    % Calculate module codelength for index i
    p = qExit(i) + pCluster;
    
    for alpha = 1 : nodesInCluster
        p_alpha_div_p_i = pi_i(cluster(alpha)) / p;
        if p_alpha_div_p_i ~= 0 % log(x)*x == NaN in MATLAB..
            HP = HP - p_alpha_div_p_i * log2(p_alpha_div_p_i);
        end
    end
    
    if qExit(i) ~= 0
        HP = HP - qExit(i) / p * log2( qExit(i) / p );
    end
    moduleCodeLength(i) = moduleCodeLength(i) + p * HP;
end


%% Calculate _indexCodeLength_
q = sum(qEnter);
HQ = 0;
for i = 1:numberOfClusters
    if qEnter(i) ~= 0
        HQ = HQ - qEnter(i)/q * log2(qEnter(i)/q);
    end
end
indexCodeLength = q * HQ;
codeLength = indexCodeLength + sum(moduleCodeLength);

