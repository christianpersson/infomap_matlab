
%% The transition matrix

% i = to, j : from, Note the transpose...
M = [0 1 0 1;
    0 0 1 0;
    1 0 0 1;
    0 0 0 0]

%% The cluster initial partition
startPartition = [1 2 3 4 5 6];

fprintf('\n\n****************************************\n')

%louvain( M , [1 2 3 4 5 6], false, true);


length = calculate( M', [1 1 2 2], false, true );
if abs(length - 2.996550064) > 0.00001
    fprintf('Error in unrec link\n');
    length
end



