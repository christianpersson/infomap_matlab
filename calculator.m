function [ codeLength, indexCodeLength, moduleCodeLength ]  = calculator( M, modules, recordedTeleportation, linkTeleportation )
%CALCULATOR A help function for calculating the code length for a
%hierarchical network. Call networkFlow to calculate the flow between nodes
%and the teleport frequency. 

    [ flow, pi, teleportP ] = networkFlow( M, recordedTeleportation, linkTeleportation );
    [ codeLength, indexCodeLength, moduleCodeLength ] = mapEquation(modules, flow, pi, teleportP, recordedTeleportation);

end

