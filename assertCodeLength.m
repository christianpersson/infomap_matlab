function [ result ] = assertCodeLength( M, partition, recordedTeleportation, linkTeleportation, expected_codeLength, tol )
%ASSERTCODELENGTH
% Help function to test the code length. 
[ codeLength, indexCodeLength, moduleCodeLength ]  = calculator( M, partition, recordedTeleportation, linkTeleportation );


fprintf('Test module: ');

printModule(partition);
fprintf('\n\t==> Calculate mapequation with ');
if recordedTeleportation
    fprintf('RECORDED')
else
    fprintf('UNRECORDED');
end
fprintf(' teleportation to ')
if(linkTeleportation)
    fprintf('LINKS')
else
    fprintf('NODES')
end

fprintf('\n\t==> ');

if abs(codeLength - expected_codeLength) > tol
    result = false;
    fprintf('Test failed \t :(');
else
    fprintf('Test succeed\t :)');
    result = true;
end
fprintf('\n\t==> %.3f (%.0e) <---> ', expected_codeLength, tol);
printCode(codeLength, indexCodeLength, moduleCodeLength);

end


function printModule(module)

fprintf('[ ');
for i = 1 : length(module)
    fprintf('%d ', module(i));
end
fprintf(']');

end


function printCode( codeLength, indexCodeLength, moduleCodeLength)
fprintf('%.3f (%.3f + %.3f)\n', codeLength, indexCodeLength, sum(moduleCodeLength));
end

