# Infomap MATLAB #

## Core files

mapEquation.m

networkFlow.m

pageRank.m

calculator.m

louvain.m (This function is not updated to work with the map equation code)

## Test helper file

assertCodeLength.m


## Script that define a map and run the map equation to calculate code length for a given partition. 

map_eigenImpact.m

map_dangling.m (not updated with the new tests)

map_sourceSink_d.m (not updated with the new tests)
