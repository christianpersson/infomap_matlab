function [ flow, p, teleportP ] = networkFlow( M, recorded, link )
%% NetworkFlow
% Given a matrix M, where $m_{ij}$ is the weigth on the edge from $j$ to
% $i$ this function returns the flow, $Q$, where $q_{ij}$ is the flow from
% node $i$ to $j$.
% Help function to prepare for the map equation. 
% This code is running once before the infomap optimization begin.

%%% Initialization
ALPHA = 0.15;
W = M';
N = length(W);
w_out = sum(W, 2);
w_in = sum(W);
w_tot = sum(sum(W));


%% Calculate teleport vector
% Determine the teleport probability to each node. The probabilities
% depends on how the visit frequency is counted. 
%
% For more information, see Lambiotte, Rosvall; _Ranking and clustering of nodes in networks with smart teleportation_
% <http://www.mapequation.org/assets/publications/LambiotteRosvall2012.pdf>
%
% If teleportation to *nodes*:
% 
% $$v_i = \frac{1}{N}$$
%
% If teleportation to *edges recorded*:
%
% $$v_i = w^{in}_i / W$$ 
%
% If teleportation to *edges unrecorded*:
% 
% $$v_i = w^{out}_i / W$$ 
 
teleportP = ones(N, 1) / N;
if link && recorded
    teleportP = w_in/w_tot;
elseif link
    teleportP = w_out/w_tot;
end


%% Calculate visit frequency for nodes with _Page Rank algorithm_
[ p, T ] = pageRank( M, teleportP );


%% Calculate probability that a random walker steps from node _i_ to node _j_
%
% $$p_{ij} = \frac{W_{ij}}{\sum_j W_{ij}}$$ 

P = zeros(N);
for alpha = 1 : N
    if sum(W(alpha,:)) == 0
        if recorded 
            P(alpha, :) = teleportP;
        end
    else
        P(alpha,:) = W(alpha,:) / sum(W(alpha,:));
    end
end


%% Calculate edge visit rate
%
% $$q_{ji} = p_jp{ji}$$ 
Q = zeros(N);
for alpha = 1 : N
    for beta = 1 : N
        Q(beta, alpha) = Q(beta, alpha) + P(beta, alpha) * p(beta);
    end
end
Q = Q / sum(sum(Q)); 


%% Update node visit rate for unrecorded teleportation 
% Updated by following the transition matrix one step and normalize.
if ~recorded 
    p =  T * p;
end
p = p / sum(p);

    
%% Calculate total flow from node _i_ to node _j_
flow = zeros(nnz(Q), 3);
i = 1;
for to = 1 : N
    for from = 1 : N
        if Q(from,to) ~= 0 % If there is a edge between between from and to.
            w = Q(from, to);% * (1 - ALPHA); %% SKa alpha va med i unrec?
            if recorded
                w = w * (1-ALPHA);
            end
            flow(i,:) = [from, to, w];
            i = i + 1;
        end
    end
end


