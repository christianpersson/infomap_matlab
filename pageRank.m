function [ p, T ] = pageRank( W, v)
%% PAGE RANK
% M: transition matrix, $M_ij$ is the weight of the link from $j$ to $i$.
alpha = 0.15;
EPS = 0.0000000000001;
MAX_ITERATIONS = 10000;

N = length(W);
w_out = sum(W);
T_teleport = W;
T = W;

%% The starting distribution
p = ones(N, 1) / N;


%% Calcute the transition matrices. 
% $T_teleport$ is the transition matrix when the dangling nodes is
% included. 
for i = 1 : N
    if w_out(i) == 0 % dangling node. 
        T_teleport(:,i) = v;  
    else
        T_teleport(:,i) = T_teleport(:,i) / w_out(i);  
        T(:,i) = T_teleport(:,i);
    end
end


%% Calculate the steady state solution for the visit distribution.
for i = 1:MAX_ITERATIONS
    vOld = p;
    p = T_teleport * p * (1 - alpha);
    
    % Add teleportation probability.
    for j = 1:length( p )
        p(j) = p(j) + alpha * v(j);
    end
    
    p = p / sum( p ); % normalize
    
    if sumabs( vOld - p ) < EPS
        %fprintf('Converged after %d iterations.\n', i)
        break;
    end
    
end

p = p / sum(p);

