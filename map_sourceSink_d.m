clear;
%SOURCESINK_D

%% unrec link:  2.970950594
%% rec link: 2.970950594

%% unrec node: 2.979868757
%% rec node: 3.93210057
clear;

M = zeros(16);
M(2,1) = 1;
M(2,3) = 1;
M(4,3)  = 1;
M(4,1) = 1;

M(2,8) = 1;

M(5,6) = 1;
M(7,6) = 1;
M(7,8) = 1;
M(5,8) = 1;

M(7,9) = 1;

M(10,9) = 1;
M(10,11) = 1;
M(12,11) = 1;
M(12,9) = 1;

M(12,14) = 1;

M(13,14) = 1;
M(15,14) = 1;
M(15,16) = 1;
M(13,16) = 1;

M(13,3) = 1;
M = M';

startPartition = [1 1 1 1 2 2 2 1 3 4 4 4 5 5 5 5];



%% Calculate the node frequency distribution
fprintf('\n****************************************\n')

assertCodeLength( M, ones(16,1), false, true, 2.970950594, 0.00001 );

assertCodeLength( M, ones(16,1), false, false, 2.979868757, 0.00001 );

assertCodeLength( M, ones(16,1), true, true, 2.970950594, 0.00001 );

assertCodeLength( M, ones(16,1), true, false, 3.93210057, 0.00001 );

assertCodeLength( M, startPartition, false, true, 2.054, 0.01 );

assertCodeLength( M, [1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4], true, false, 4.58, 0.01 );
