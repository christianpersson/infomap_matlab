function [ best, codeLength ] = louvain( W , startPartition, recorded, linkTeleportation )

%% WARNING: This function is not updated to match the 

error('Warning: This function is depricated for the moment. It is not updated to match the current API')

NR_OF_ITERATIONS = 100;
ALPHA = 0.15;
N = length(W);

printInfo(N, recorded, linkTeleportation);

[ p, v, T ] = pageRank(W, ALPHA);
nodeFrequencies = T * p / sum(T*p);
pp = p / sum(p);

printFreq(nodeFrequencies);

printModule(W, 1:N, recorded, linkTeleportation)
printModule(W, ones(N,1), recorded, linkTeleportation)
printModule(W, startPartition, recorded, linkTeleportation)

%% Optimize map equation
best = startPartition;
for i = 1 : NR_OF_ITERATIONS
    
    modules = startPartition;
    update = true;
    codeLength = mapEquation(W, modules, recorded, linkTeleportation);
    
    while update == true
        update = false;
        for from = randperm(N) %% for all nodes
            for to = 1 : N
                if W(to, from) ~= 0 %% if it is connected to this node
                    temp = modules;
                    temp(from) = modules(to); %% set this node in same cluster as the other
                    newCodeLen = mapEquation(W, temp, recorded, linkTeleportation);
                    if  newCodeLen < codeLength
                        modules = temp;
                        codeLength = newCodeLen;
                    end
                end
            end
        end
    end
    
    if mapEquation(W, modules, recorded, linkTeleportation) < mapEquation(W, best, recorded, linkTeleportation)
        best = modules;
    end
    
end


%% dump the best partition with corresponding code length
printModule(W, best, nodeFrequencies, recorded, linkTeleportation,pp)
end


function printModule(W, partition, recorded, linkTeleportation)

    [ codeLength, indexCodeLength, moduleCodeLength ] = mapEquation(W, partition, recorded, linkTeleportation);
    fprintf('Module: [ ')
    for j = partition
        fprintf('%d ', j)
    end
    fprintf('] ==> %.3f + %.3f = %.3f\n', indexCodeLength, moduleCodeLength, codeLength);

end

function printFreq(freq)
    fprintf('Freqs: ');
    for i = 1 : length(freq)
        fprintf('%.3f, ', freq(i));
    end
    fprintf('\n');
end

function printInfo(N, recorded, linkTeleportation)
    fprintf('\n\n===================== ')
    if(recorded)
        fprintf('recorded')
    else
         fprintf('unrecorded')
    end
    fprintf(' teleportation to ')
    if(linkTeleportation)
        fprintf('links.')
    else
        fprintf('nodes')
    end
    fprintf('\n')
    fprintf('%d nodes. \n', N);

    fprintf('Calculate page rank: ')
end

